import logging
import os
import re
import shutil
from functools import wraps
from pathlib import Path

DEBUG_LEVEL = "INFO"
BUILD_DIR = "build"


class MyLogger:
    def __init__(self):
        self.log_levels = {
            "debug": logging.DEBUG,
            "info": logging.INFO,
            "warning": logging.WARNING,
            "warn": logging.WARN,
        }

    @property
    def _get_handler(self):
        handler = logging.StreamHandler()
        handler.setFormatter(
            logging.Formatter("%(levelname)s | %(asctime)s | [%(name)s] | %(message)s")
        )
        return handler

    def get_logger(self, name=None, level=None):
        if level:
            level = level.lower()
        log_level = self.log_levels.get(level) or logging.DEBUG
        logger = logging.getLogger(name)
        logger.addHandler(self._get_handler)
        logger.setLevel(log_level)
        return logger


class NotValidTagException(Exception):
    """Raised if tag has wrong format. Only v##.##.## tag template is accepted"""

    pass


class Builder:
    def __init__(self, name=None):
        self.name = name or __name__
        self.logger = MyLogger().get_logger(self.name, DEBUG_LEVEL)
        self.build_dir = BUILD_DIR

    def is_valid_tag(self, tag):
        return re.match("v\d+.\d+.\d+$", tag)


def copy_files(files: list, destination: Path) -> None:
    for each_file in files:
        shutil.copy(each_file, str(destination.as_posix()))


def clear_folder(folder) -> None:
    for root, dirs, files in os.walk(folder):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))
