import sys
import zipfile
from pathlib import Path

from ci_utils import Builder, NotValidTagException

SEARCH_DIR = "Atoms"


class BuildAtoms(Builder):
    def get_files_by_extension(self, folder, ext=None) -> list:
        if not ext:
            print("No extension provided, aborted")
            return
        files = []
        for name in folder.rglob(ext):
            files.append(name)
        return files

    def build_atoms_archive(self, tag_name, archive_format=None, dry_run=None):
        """
        Take the tag name, validate it
        Copy all .atom files in the SEARCH_DIR folder to the build_dir
        Create acrchive from the build_dir folder contents in the root directory
        Clear the build_dir folder and move archive there for CI/CD processing
        """

        if not self.is_valid_tag(tag_name):
            raise NotValidTag(f"Tag {tag_name} is not valid. Use `v#.#.#` format")

        if not archive_format:
            archive_format = "zip"
        search_folder = Path(SEARCH_DIR).resolve()
        output_folder = Path(self.build_dir).resolve()
        output_file = Path(output_folder, f"atoms_archive_{tag_name}.{archive_format}")
        atom_files = self.get_files_by_extension(search_folder, "*.atom")
        if dry_run:
            self.logger.info("Dry run processing...")
            self.logger.info(f"Build directory: [{self.build_dir}]")
            self.logger.info(f"Search directory: [{SEARCH_DIR}]")
            self.logger.info(f"[{output_folder.name}] folder will be is created")
            self.logger.info(
                f"Atom files archive will be created in [{output_folder.name}] folder is not exists"
            )
            return
        self.logger.debug(f"Tag name is used: {tag_name}")
        output_folder.mkdir(exist_ok=True, parents=True)
        self.logger.debug(f"[{output_folder.name}] folder is created")
        if not atom_files:
            return
        with zipfile.ZipFile(output_file, "w", zipfile.ZIP_DEFLATED) as zf:
            for atom in atom_files:
                zf.write(atom, atom.name)
        self.logger.debug(
            f"Atom files archive is created in [{output_folder.name}] folder"
        )


if __name__ == "__main__":
    builder = BuildAtoms("Build Atoms Script")
    try:
        tag_name = sys.argv[1]
        builder.build_atoms_archive(tag_name)
    except IndexError:
        builder.build_atoms_archive("v99.0.0", archive_format="zip", dry_run=False)
