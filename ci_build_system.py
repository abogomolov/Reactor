import os
import sys
import zipfile
from pathlib import Path

from ci_utils import Builder, NotValidTagException

SEARCH_DIR = "System"


class BuildSystem(Builder):
    def build_reactor_system_archive(self, tag_name, archive_format=None, dry_run=None):
        """
        Make archive from System folder contents
        """
        if not self.is_valid_tag(tag_name):
            raise NotValidTag(f"Tag {tag_name} is not valid. Use `v#.#.#` format")

        if not archive_format:
            archive_format = "zip"
        search_folder = Path(SEARCH_DIR).resolve()
        output_folder = Path(self.build_dir).resolve()
        output_file = Path(
            output_folder, f"reactor_system_archive_{tag_name}.{archive_format}"
        )
        if dry_run:
            self.logger.info("Dry run processing...")
            self.logger.info(f"Build directory: [{self.build_dir}]")
            self.logger.info(f"Search directory: [{SEARCH_DIR}]")
            return
        output_folder.mkdir(exist_ok=True, parents=True)

        with zipfile.ZipFile(output_file, "w", zipfile.ZIP_DEFLATED) as zf:
            for file_path in search_folder.rglob("*.*"):
                rel_path = file_path.relative_to(search_folder)
                zf.write(file_path, rel_path)
        self.logger.debug(
            f"Reactor system files archive is created in [{output_folder.name}] folder"
        )


if __name__ == "__main__":
    builder = BuildSystem("Build System Script")
    try:
        tag_name = sys.argv[1]
        builder.build_reactor_system_archive(tag_name)
    except IndexError:
        builder.build_reactor_system_archive("v99.0.0", dry_run=False)
